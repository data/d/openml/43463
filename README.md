# OpenML dataset: Insurance-Premium-Data

https://www.openml.org/d/43463

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This Dataset is something I found online when I wanted to practice regression models. It is an openly available online dataset at multiple places. Though I do not know the exact origin and collection methodology of the data, I would recommend this dataset to everybody who is just beginning their journey in Data science.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43463) of an [OpenML dataset](https://www.openml.org/d/43463). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43463/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43463/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43463/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

